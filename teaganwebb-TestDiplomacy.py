# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import dip_eval, dip_read, dip_solve, dip_print

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):


    # ----
    # dip_read
    # ----

    def test_read_1(self): 
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
        t = dip_read(s)
        u = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        self.assertEqual(t, u)


    def test_read_2(self): 
        s = "A Madrid Hold\nB Barcelona Move Madrid"
        t = dip_read(s)
        u = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']]
        self.assertEqual(t, u)

    def test_read_3(self): 
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
        t = dip_read(s)
        u = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'],  ['D', 'Paris', 'Support', 'B'],  ['E', 'Austin', 'Support', 'A']]
        self.assertEqual(t, u)


    # ----
    # dip_eval
    # ----

    # normal/failure case, to make sure our unit tests are working!
    def test_eval_1(self):
        s = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        t = dip_eval(s)
        u = "A [dead]\nB Madrid\nC London\n"
        self.assertEqual(t, u)

    # corner case -- everyone dies
    def test_eval_2(self): 
    	s = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']]
    	t = dip_eval(s)
    	u = "A [dead]\nB [dead]\n"
    	self.assertEqual(t, u)

   	#corner case -- super weird tie case
    def test_eval_3(self): 
    	s = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'],  ['D', 'Paris', 'Support', 'B'],  ['E', 'Austin', 'Support', 'A']]
    	t = dip_eval(s)
    	u = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
    	self.assertEqual(t, u)


    # ----
    # dip_print
    # ----

    def test_print_1(self): 
        s = "A [dead]\nB Madrid\nC London\n"
        w = StringIO()
        t = dip_print(w, s)
        u = "A [dead]\nB Madrid\nC London\n"
        self.assertEqual(w.getvalue(), u)

    def test_print_2(self): 
        s = "A [dead]\nB [dead]\n"
        w = StringIO()
        t = dip_print(w, s)
        u = "A [dead]\nB [dead]\n"
        self.assertEqual(w.getvalue(), u)

    def test_print_3(self): 
        s = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        w = StringIO()
        t = dip_print(w, s)
        u = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        self.assertEqual(w.getvalue(), u)    

    # ----
    # dip_solve
    # ----

    def test_solve_1(self): 
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        dip_solve(r, w)
        u = "A [dead]\nB Madrid\nC London\n"
        self.assertEqual(w.getvalue(), u)

    def test_solve_2(self): 
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        dip_solve(r, w)
        u = "A [dead]\nB [dead]\n"
        self.assertEqual(w.getvalue(), u)

    def test_solve_3(self): 
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        dip_solve(r, w)
        u = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        self.assertEqual(w.getvalue(), u)


if __name__ == "__main__" : 
    main() 
