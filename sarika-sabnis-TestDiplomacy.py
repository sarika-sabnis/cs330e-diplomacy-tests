from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestCollatz
# -----------


class TestDiplomacy(TestCase):
    """
    This is the class to test the Diplomacy test cases.
    Add in functions as you add in test cases.
    """

    # ----
    # read
    # ----
    def test_read_1(self):
        strin = StringIO("A Madrid Hold\nB Paris Move Madrid")
        armylist = diplomacy_read(strin)
        self.assertEqual(armylist, [["A", "Madrid", "Hold"],["B", "Paris", "Move", "Madrid"]])

    # --------
    # evaluate
    # --------
    def test_eval_1(self):
        final = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Paris", "Move", "Madrid"], ["C", "London", "Support", "B"]])
        self.assertEqual(final, {"A": "[dead]", "B": "Madrid", "C": "London"})

    # -----
    # print
    # -----
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]", "B": "Madrid", "C": "London"})
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # ------
    # result
    # ------
    def test_result_1(self):
        r =StringIO("A Madrid Hold\nB Paris Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_result_2(self):
        r = StringIO("A Madrid Hold\nB Paris Move Madrid\nC London Support B\nD Tokyo Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Tokyo\n")

    def test_result_3(self):
        r = StringIO("A Madrid Hold\nB Paris Move Madrid\nC London Move Madrid\nD Spain Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD London\n")



# ----
# main
# ----

if __name__ == "__main__":
    main()
