from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
	
	def test_solve_1(self):
		r = StringIO("A Madrid Hold")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Madrid\n")

	def test_solve_2(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC London\n")

	def test_solve_3(self):
		r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

	def test_solve_4(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

if __name__ == "__main__":
    main()