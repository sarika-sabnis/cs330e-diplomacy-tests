# Write unit tests in TestDiplomacy.py that test corner cases and failure cases until
# you have an 3 tests for the function diplomacy_solve(), confirm the expected failures,
# and add, commit, and push to the private code repo.

#!/usr/bin/env python3

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_print, diplomacy_eval

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    # ----
    # read
    # ----

    # diplomacy_read takes the input, reads all the initial conditions, and makes a list of the armies in their current states


    def test_solve_1(self):
    	r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n C London Support B\n D Austin Move London\n")
    	w = StringIO()
    	diplomacy_solve(r,w)
    	self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
    	r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n C London Move Madrid\n D Paris Support B\n")
    	w = StringIO()
    	diplomacy_solve(r,w)
    	self.assertEqual(w.getvalue(),"A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3(self):
    	r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n C London Move Madrid\n D Paris Support B\n E Austin Support A\n")
    	w = StringIO()
    	diplomacy_solve(r,w)
    	self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""